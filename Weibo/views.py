from django.shortcuts import render
from django.http import HttpResponse
import json
from Weibo import models


def hello(request):
    return HttpResponse("Hello world ! ")


def home(request):
    # return HttpResponse("Hello world ! ")
    testlist = ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"]

    weatherType = models.weather.objects.all().distinct().values_list('type')
    weatherTypeList = [x[0] for x in weatherType]
    weatherTypeNumber = [models.weather.objects.all().values('type').filter(type=x).count() for x in weatherTypeList]
    weatherTypeDic = [{'value': weatherTypeNumber[x],
                       'name': weatherTypeList[x]}
                      for x in range(len(weatherTypeList))]

    return render(request, 'home.html', {'TutorialList': testlist,
                                         'Jsondict':json.dumps({'value':5, 6:6}),
                                         'WeatherTypeList':weatherTypeList,
                                         'WeatherTypeDic':weatherTypeDic,
                                         'WeatherTypeNumber':weatherTypeNumber
                                         })
