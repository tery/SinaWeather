# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import urljoin, urlparse, parse_qs
from weibo0705.items import Weibo0705Item
import os
from datetime import datetime
import dateutil.parser
import re


def pause():
    os.system("pause")


class ASpider(scrapy.Spider):
    name = 'a'
    start_urls = ['https://weibo.cn/u/5112656389?page=1']
    headers = {
        'Connection': 'keep - alive',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36'
    }
    cookie = {'ALF': '1533216295', '_T_WM': 'fe3e6c3fd1a13d03af088f54967cc3ae',
              'SCF': 'AppY4TkoriqEKYvZ3BYkQHvpA_BmL78c9cexNOBgpZSJJ1B7dkVypzKa7kHuFAFMD1EMjvoGu2e9Krpz9ikR5Io.',
              'SUB': '_2A252PwxhDeRhGeVJ7lMV8CfKwzmIHXVVw5QprDV6PUJbktAKLRf8kW1NT_FwAlm7Mu3VeFXMfPiu9b_n5AQGV1Ey',
              'SUBP': '0033WrSXqPxfM725Ws9jqgMF55529P9D9WhpW7lPPvv5iNCjIQ1UuVCB5JpX5K-hUgL.FoeNSK2Xeh.c1h-2dJLoI0YLxKMLBK2LB-zLxK.L1h-LBonLxKBLBonL1h5LxKML1hzLBo.LxK.L1heLB.BLxKBLBonL12BLxKML1KBL1-et',
              'SUHB': '0eb9W2MqNI4GHt'}

    def start_requests(self):
        yield scrapy.Request(url=self.start_urls[0], headers=self.headers, cookies=self.cookie)

    def parse(self, response):
        with open("check.html", "wb") as f:
            f.write(response.body)
        f.close()
        # titles=response.xpath('//*[@class="c"]/div/span/text()').extract()

        titles = response.xpath('//*[@class="c"]/div/span[@class="ctt"]/text()').extract()
        titles = list(filter(lambda x: x != ' \u200b\u200b\u200b', titles))

        times = response.xpath('//*[@class="ct"]/text()').extract()
        times = list(map(lambda x: x.replace('\xa0来自天气通', ''), times))
        print(titles)
        print(times)

        for title, time in zip(titles, times):
            print(title)
            print(time)
            new_item = Weibo0705Item()
            if title[3] == '今':
                new_item['type'] = title[5:7]
                new_item['location'] = title[0:2]
                new_item['add_time'] = dateutil.parser.parse(time, fuzzy=True).strftime("%Y-%m-%d %H:%M:%S")
            else:
                continue

            yield new_item

        urls = response.xpath('//*[@id="pagelist"]/form/div/a[1]/@href').extract()
        next_page = urljoin(self.start_urls[0], urls[0])
        print(next_page)
        page = int(parse_qs(urlparse(next_page).query)['page'][0])
        if page >= 10:
            return None
        yield scrapy.Request(next_page, callback=self.parse)
